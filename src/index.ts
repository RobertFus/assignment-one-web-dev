import Mongoose from "mongoose";
import { User } from "./persistance/User";

//----------------------------------------------------------------------------------------------------------------------
(async () => {
    await Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });
    const user = await new User({ userAddress: '300 Boston Post Rd', userCity: 'West Haven', userState: 'Connecticut'});
    user.fullAddress = "300 Boston Post Rd-West Haven-Connecticut";
    const savedUser = await user.save();
    console.log(savedUser);
    process.exit(0);
})();