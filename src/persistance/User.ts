import Mongoose from 'mongoose';
import { IUser } from "../models/IUser";

const userSchema: Mongoose.Schema<IUser> = new Mongoose.Schema<IUser> ({

//---------------------------------------------------------------------------------------------------------------------
    userName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 6;
            },
            message: 'User name must contain more than 6 characters'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    firstName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'First name must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    lastName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Last Name must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    password: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z0-9!@#$%^&*]*$/.test(value);
            },
            message: 'Password must meet criteria a-z A-Z 0-9 !@#$%^&*'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userAddress: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0;
            },
        },
        message: 'User address must be larger than 0'
    },

//---------------------------------------------------------------------------------------------------------------------
    userCity: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'City Must be a letter'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userState: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'State must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userZipcode: {
        type: Number,
        validate: {
            validator: function (value: number): boolean {
                return value > 0 && value < 6;
            },
            message: 'Zipcode must be 5 digits'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userType: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Type must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userId: {
        type: Number,
        required: true,
        validate: {
            validator: function (value: number): boolean {
                return value > 0;
            },
            message: 'Id must be a number greater than zero.'
        }
    }
});

//----------------------------------------------------------------------------------------------------------------------
userSchema.virtual('fullAddress')
    .get(function (this: IUser) {
        return this.userAddress + ' ' + this.userCity + ' ' + this.userState;
    }).set(function (this: IUser, fullAddress: string) {
        if (!fullAddress.includes('-')) {
            throw new Error('Full name must have a hyphen between the first and last name');
        }
        const [userAddress, userCity, userState]: string[] = fullAddress.split('-');
        this.userAddress = userAddress;
        this.userCity = userCity;
        this.userState = userState;
    }
);
export const User = Mongoose.model<IUser>('user', userSchema);