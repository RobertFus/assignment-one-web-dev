import { Document } from 'mongoose';

/*
* This is the Service Model, this is built so that the user can choose a certain type of service needed for their project
* Service is broken up into multiple different fields for the time of the year.
 */
export interface IServiceTypes extends Document {

    serviceId: number,
    snowService: string,
    grassService: string,
    lawnService: string,
    estimatedCost: number

}
