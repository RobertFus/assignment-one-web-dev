import { Document } from 'mongoose';
/*
* This is the User model. All the users information will be stored into these fields. There is an user Type to determine
* what type of user there is to determine what privileges they have.
 */
export interface IUser extends Document {

    userName: string,
    firstName: string,
    lastName: string,
    password: string,
    userAddress: string,
    userCity: string,
    userState: string,
    userZipcode: number,
    userType: string,
    fullAddress: string,
    userId: number

}
