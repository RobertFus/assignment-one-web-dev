const { ServiceTypes } = require('../build/persistance/ServiceTypes');

describe('User model tests', function() {

    before(async function () {
        await Mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});
    });

    beforeEach(async function () {
        await ServiceTypes.deleteMany({});
    });
// ID Validation to prevent ID To have a letter.
//---------------------------------------------------------------------------------------------------------------------
    describe('ID Validation Test', function() {
        it('Prevent ID to be a letter', async function() {
            const service = await new ServiceTypes({ serviceId: 'Aj15325' });
            try {
                await service.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('Service validation failed: ID can not be a letter');
            }
        });
    });
//
//---------------------------------------------------------------------------------------------------------------------
    describe('Snow Service validation test', function() {
        it('See if snow service works', async function() {
            const service = await new ServiceTypes({ snowService: 'SERVICESNOW' });
            expect(service.snowService).to.equal('SERVICESNOW');
            service.snowService = 'Snow Service';
            expect(service.snowService).to.equal('Snow Service');
            const savedUser = await service.save();
            expect(savedUser.snowService).to.equal('Snow Service');
        });
    });
//Prevent Cost to have letters within it.
//---------------------------------------------------------------------------------------------------------------------
    describe('Cost Validation Test', function() {
        it('Prevent Cost to be a letter', async function() {
            const service = await new ServiceTypes({ estimatedCost: 'ABC' });
            try {
                await service.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('Service validation failed: Cost can not be a letter');
            }
        });
    });
//Test to see if a negative number is allowed
//---------------------------------------------------------------------------------------------------------------------
    describe('Cost Validation Test', function() {
        it('Prevent Cost to be a negative number', async function() {
            const service = await new ServiceTypes({ estimatedCost: '-1' });
            try {
                await service.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('Service validation failed: Cost can not be a negative number');
            }
        });
    });
// Test to see if it prevents over 300 characters
//---------------------------------------------------------------------------------------------------------------------
    describe('Lawn Service Validation Test', function() {
        it('Prevent Length to be over 300', async function() {
            const service = await new ServiceTypes({ lawnService: 'abcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyz' +
                    'abcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyz' +
                    'abcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyz' +
                    'abcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyz' });
            try {
                await service.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('Service validation failed: Too Long');
            }
        });
    });
});