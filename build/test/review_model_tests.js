"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var ReviewContractor = require('../build/persistance/ReviewContractor').ReviewContractor;
describe('Review model tests', function () {
    before(function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    beforeEach(function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ReviewContractor.deleteMany({})];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    //Test to see if rating system works properly
    //---------------------------------------------------------------------------------------------------------------------
    describe('Rating Validation Test', function () {
        it('Allow for rating to be between 1-5', function () {
            return __awaiter(this, void 0, void 0, function () {
                var review, savedUser;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new ReviewContractor({ customerRating: 'RATING' })];
                        case 1:
                            review = _a.sent();
                            expect(review.customerRating).to.equal('RATING');
                            review.customerRating = '5';
                            expect(review.customerRating).to.equal('5');
                            return [4 /*yield*/, review.save()];
                        case 2:
                            savedUser = _a.sent();
                            expect(savedUser.customerRating).to.equal('5');
                            return [2 /*return*/];
                    }
                });
            });
        });
    });
    //Test to prevent rating to go above 5
    //---------------------------------------------------------------------------------------------------------------------
    describe('Rating Validation Test', function () {
        it('Prevent Rating to be above 5', function () {
            return __awaiter(this, void 0, void 0, function () {
                var review, error_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new ReviewContractor({ customerRating: '10' })];
                        case 1:
                            review = _a.sent();
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 4, , 5]);
                            return [4 /*yield*/, review.save()];
                        case 3:
                            _a.sent();
                            expect.fail("Expected error not thrown");
                            return [3 /*break*/, 5];
                        case 4:
                            error_1 = _a.sent();
                            expect(error_1.message).to.equal('Review validation failed: Rating Above 5');
                            return [3 /*break*/, 5];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        });
    });
    //Test to prevent Rating to be a letter
    //---------------------------------------------------------------------------------------------------------------------
    describe('Rating Validation Test', function () {
        it('Prevent Rating to be a letter', function () {
            return __awaiter(this, void 0, void 0, function () {
                var review, error_2;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new ReviewContractor({ customerRating: 'ABC' })];
                        case 1:
                            review = _a.sent();
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 4, , 5]);
                            return [4 /*yield*/, review.save()];
                        case 3:
                            _a.sent();
                            expect.fail("Expected error not thrown");
                            return [3 /*break*/, 5];
                        case 4:
                            error_2 = _a.sent();
                            expect(error_2.message).to.equal('Review validation failed: Rating can not be a letter');
                            return [3 /*break*/, 5];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        });
    });
    // Comment validation test to make sure that the characters stay within 300 characters to prevent large build up
    //---------------------------------------------------------------------------------------------------------------------
    describe('Comment Validation Test', function () {
        it('Comment Must be inbetween 300 characters', function () {
            return __awaiter(this, void 0, void 0, function () {
                var review, savedUser;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new ReviewContractor({ customerComment: 'Comment' })];
                        case 1:
                            review = _a.sent();
                            expect(review.customerComment).to.equal('Comment');
                            review.customerComment = 'THIS IS A COMMENT TO MAKE SURE IT IS UNDER 300 CHARACTERS';
                            expect(review.customerComment).to.equal('THIS IS A COMMENT TO MAKE SURE IT IS UNDER 300 CHARACTERS');
                            return [4 /*yield*/, review.save()];
                        case 2:
                            savedUser = _a.sent();
                            expect(savedUser.customerComment).to.equal('THIS IS A COMMENT TO MAKE SURE IT IS UNDER 300 CHARACTERS');
                            return [2 /*return*/];
                    }
                });
            });
        });
    });
    // ID Validation to prevent ID To have a letter.
    //---------------------------------------------------------------------------------------------------------------------
    describe('ID Validation Test', function () {
        it('Prevent ID to be a letter', function () {
            return __awaiter(this, void 0, void 0, function () {
                var review, error_3;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new ReviewContractor({ customerRating: 'ABC15325' })];
                        case 1:
                            review = _a.sent();
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 4, , 5]);
                            return [4 /*yield*/, review.save()];
                        case 3:
                            _a.sent();
                            expect.fail("Expected error not thrown");
                            return [3 /*break*/, 5];
                        case 4:
                            error_3 = _a.sent();
                            expect(error_3.message).to.equal('Review validation failed: Rating can not be a letter');
                            return [3 /*break*/, 5];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        });
    });
});
