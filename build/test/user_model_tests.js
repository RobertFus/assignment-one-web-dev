"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var chai = require('chai');
var expect = chai.expect;
var Mongoose = require('mongoose');
var User = require('../build/persistance/User').User;
//---------------------------------------------------------------------------------------------------------------------
describe('User model tests', function () {
    before(function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    beforeEach(function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, User.deleteMany({})];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    // Make address into a full address.
    //---------------------------------------------------------------------------------------------------------------------
    describe('Full Address  virtual test', function () {
        it('Allow for full address to be set virtual', function () {
            return __awaiter(this, void 0, void 0, function () {
                var user, savedUser;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new User({ userAddress: 'ADDRESS', userCity: 'CITY', userState: 'STATE' })];
                        case 1:
                            user = _a.sent();
                            expect(user.userAddress).to.equal('ADDRESS');
                            expect(user.userCity).to.equal('CITY');
                            expect(user.userState).to.equal('STATE');
                            user.fullAddress = '300 Boston Post Rd-West Haven-Connecticut';
                            expect(user.userAddress).to.equal('300 Boston Post Rd');
                            expect(user.userCity).to.equal('West Haven');
                            expect(user.userState).to.equal('Connecticut');
                            return [4 /*yield*/, user.save()];
                        case 2:
                            savedUser = _a.sent();
                            expect(savedUser.userAddress).to.equal('300 Boston Post Rd');
                            expect(savedUser.userCity).to.equal('West Haven');
                            expect(savedUser.userState).to.equal('Connecticut');
                            return [2 /*return*/];
                    }
                });
            });
        });
    });
    // Zipcode validation to make sure zipcode is within 5 digits in length
    //---------------------------------------------------------------------------------------------------------------------
    describe('Zipcode validation test', function () {
        it('Allow for zipcode to be 5 numbers', function () {
            return __awaiter(this, void 0, void 0, function () {
                var user, savedUser;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new User({ userZipcode: 'ZIPCODE' })];
                        case 1:
                            user = _a.sent();
                            expect(user.userZipcode).to.equal('ZIPCODE');
                            user.userZipcode = '06418';
                            expect(user.userZipcode).to.equal('06418');
                            return [4 /*yield*/, user.save()];
                        case 2:
                            savedUser = _a.sent();
                            expect(savedUser.userZipcode).to.equal('06418');
                            return [2 /*return*/];
                    }
                });
            });
        });
    });
    //Allows for the user type to have any alphabetic character
    //---------------------------------------------------------------------------------------------------------------------
    describe('User Type validation test', function () {
        it('Allow for user type or be any alphabetic character', function () {
            return __awaiter(this, void 0, void 0, function () {
                var user, savedUser;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new User({ userType: 'ADMIN_TYPE' })];
                        case 1:
                            user = _a.sent();
                            expect(user.userType).to.equal('ADMIN_TYPE');
                            user.userType = 'Admin';
                            expect(user.userType).to.equal('Admin');
                            return [4 /*yield*/, user.save()];
                        case 2:
                            savedUser = _a.sent();
                            expect(savedUser.userType).to.equal('Admin');
                            return [2 /*return*/];
                    }
                });
            });
        });
    });
    //Prevent to username to have under 6 characters
    //---------------------------------------------------------------------------------------------------------------------
    describe('User name validation test', function () {
        it('Prevent user names under 6 characters', function () {
            return __awaiter(this, void 0, void 0, function () {
                var user, error_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new User({ userName: 'John' })];
                        case 1:
                            user = _a.sent();
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 4, , 5]);
                            return [4 /*yield*/, user.save()];
                        case 3:
                            _a.sent();
                            expect.fail("Expected error not thrown");
                            return [3 /*break*/, 5];
                        case 4:
                            error_1 = _a.sent();
                            expect(error_1.message).to.equal('user validation failed: User name under 6 Characters');
                            return [3 /*break*/, 5];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        });
    });
    //Prevent the last name of having numbers
    //---------------------------------------------------------------------------------------------------------------------
    describe('last name validation test', function () {
        it('Prevent last name to have numbers', function () {
            return __awaiter(this, void 0, void 0, function () {
                var user, error_2;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new User({ lastName: 'John123' })];
                        case 1:
                            user = _a.sent();
                            _a.label = 2;
                        case 2:
                            _a.trys.push([2, 4, , 5]);
                            return [4 /*yield*/, user.save()];
                        case 3:
                            _a.sent();
                            expect.fail("Expected error not thrown");
                            return [3 /*break*/, 5];
                        case 4:
                            error_2 = _a.sent();
                            expect(error_2.message).to.equal('user validation failed: Last Name has numbers, Not allowed');
                            return [3 /*break*/, 5];
                        case 5: return [2 /*return*/];
                    }
                });
            });
        });
    });
});
