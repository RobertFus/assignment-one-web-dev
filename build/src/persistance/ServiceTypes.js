"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
//---------------------------------------------------------------------------------------------------------------------
var serviceSchema = new mongoose_1.default.Schema({
    serviceId: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return value > 0;
            },
            message: 'The id must be greater than 0'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    snowService: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    grassService: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    lawnService: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    estimatedCost: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return value > 0;
            },
            message: 'The service cost must be greater than 0'
        }
    }
});
exports.ServiceTypes = mongoose_1.default.model('service', serviceSchema);
