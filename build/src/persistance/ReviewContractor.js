"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
//---------------------------------------------------------------------------------------------------------------------
var reviewSchema = new mongoose_1.default.Schema({
    userName: {
        type: String,
        required: true,
        //Validation so that the user had a minimum length.
        validate: {
            validator: function (value) {
                return value.length > 6;
            },
            message: 'User name must contain more than 6 characters'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    customerComment: {
        type: String,
        required: true,
        //Comments must have less than 300 characters so that there is not spam
        validate: {
            validator: function (value) {
                return value.length < 300;
            },
            message: 'Comment must be less than 300 characters'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    customerRating: {
        type: Number,
        required: true,
        //Rating must be inbetween 1 -5 for a star rating.
        validate: {
            validator: function (value) {
                return (value > 0 && value < 6);
            },
            message: 'The rating must be between 1-5'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    reviewId: {
        type: Number,
        validate: {
            validator: function (value) {
                return value > 0;
            },
            message: 'The ID can not be negative'
        }
    }
});
exports.ReviewContactor = mongoose_1.default.model('review', reviewSchema);
